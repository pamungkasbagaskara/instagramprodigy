/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import * as React from 'react';
import Home from './src/pages/home/Home'
import Navigation from './src/navigation/Navigation'

const App = () => {
  return (
    <Navigation />   
  )
}

export default App;
