import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './navigation-styles'
import Home from '../pages/home/Home';
import Favorite from '../pages/favorite/Favorite';
import Detail from '../pages/detail/Detail';
import DetailSearch from '../pages/detail/DetailSearch';

const RootStack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();


const Navigation = () => {
    return(
        <NavigationContainer independent={true}>
            <RootStack.Navigator initialRouteName="TabNavigation" screenOptions={{ headerShown: false }} >
              <RootStack.Screen name="TabNavigation" component={TabNavigation} />
              <RootStack.Screen name="Detail" component={Detail}/>
              <RootStack.Screen name="DetailSearch" component={DetailSearch}/>
            </RootStack.Navigator>
      </NavigationContainer>
    )
}
export default Navigation;

function TabNavigation() {
	return (
	  <Tab.Navigator 
        initialRouteName="Home" 
        screenOptions={{ 
          headerShown: false,
          tabBarStyle: { 
            position: 'absolute', 
            bottom : 10, 
            left : 30,
            right : 30,
            // elevation:10,
            borderRadius:15,
            height : 60,
            backgroundColor:'#EBF1F6',
            ...styles.shadow 
          },
          
         }} 
    >
        <Tab.Screen 
          name="Home"
          component={Home}
          options={{
            tabBarShowLabel :false,

            tabBarIcon: ({ focused, color, size }) => (
              <MaterialCommunityIcons name="home" color={focused? '#0A1A1D': '#A7B7C7'} size={40} />
            ),
          }}
        />
        <Tab.Screen 
          name="Favorite"
          component={Favorite}
          options={{
            tabBarShowLabel :false,
            tabBarIcon: ({ focused, color, size }) => (
              <MaterialCommunityIcons name="heart" color={focused? '#0A1A1D': '#A7B7C7'} size={40} />
            ),
          }}
        /> 
	  </Tab.Navigator>
	);
  }