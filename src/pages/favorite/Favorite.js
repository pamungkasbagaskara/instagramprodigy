import React,{ useState, useEffect } from 'react';
import {View, Text, FlatList, TouchableHighlight, Image} from 'react-native';
import getItem from '../../service/saveItem/getItem';
import styles from './Favorite-style';

function Favorite({ navigation }) {
    const [dataFav, setDataFav] = useState([]) ;
    useEffect(()=>{
        getFavData();
    }, [dataFav])

    const getFavData = async() => {
        const resp = await getItem(); 
        setDataFav(resp);
        console.log("HIT FAV")
    }


    renderItem = ({ item }) => {
		return (            
            <TouchableHighlight 
                onPress={() => navigation.navigate('Detail',{detailItem : item}) }
                style={{ flex: 1, flexDirection: 'column',margin: 1 }}>	
                <Image
                    style={{justifyContent: 'center', alignItems: 'center', height: 150,margin: 5, borderRadius:20}}
                    source={{ uri: `https://www.artic.edu/iiif/2/${item.image_id}/full/843,/0/default.jpg` }}
                />
            </TouchableHighlight>            
		);
	  }

    return (
        <View>
            <View style={styles.topContainer}>
                <Text style={styles.titleFont}>{"Saved Gallery"}</Text>                    
            </View>
            <View>
                <FlatList
                    data={dataFav}
                    renderItem={renderItem}
                    numColumns={3}
                    keyExtractor={item => item.id}
                />
            </View>
        </View>
    )
}

export default Favorite;