import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
      topContainer :{
        backgroundColor: '#0A1A1D',
        borderBottomEndRadius : 10,
        borderBottomLeftRadius : 10,
      },
      titleFont :{
          fontSize: 30,
          color:'#fff',
          alignSelf : 'center',
          marginTop: 10,
          marginBottom:10,
      }

});
export default styles;
