import { Dimensions, StyleSheet } from 'react-native';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#De3030'
      },
    fontHalo: {
        fontSize: 20,
        alignSelf: 'center',
        marginLeft: 10,
        fontWeight : 'bold'
        },
    image : {
        width : 0.75 * width,
        height : 0.75 * width,
        alignSelf: 'center',
        borderRadius : 20,   
        marginBottom : 20     
    },
    fontSubTitle : {
        fontSize: 20,
        color : 'black',
        fontWeight: 'bold',
        alignSelf: "center",
    },
    fontSub : {
        fontSize: 16,
        color : 'black',
        fontFamily : 'sans-serif-condensed',

    },
    textContainer : {
        padding : 0.05 * width
    },
    textSubContainer : {
        marginBottom : 10
    },
    favContainer : {
        marginLeft : 25, 
        alignContent:'center',
        alignItems:'center', 
        backgroundColor:'#cfe1b9',
        alignContent:'center',
        width:37,
        height:37,
        borderRadius:50,
        justifyContent:'center'
    },
    creditContainer : {
        flexDirection:'row', 
        justifyContent:'space-between', 
        paddingTop : 0.005 * width,
        paddingRight : 0.05 * width,
        paddingBottom : 0.005 * width,
        marginBottom : 10,
    },
    topContainer :{
        backgroundColor: '#606c38',
        borderBottomEndRadius : 10,
        borderBottomLeftRadius : 10,
      },
      titleFont :{
          fontSize: 30,
          color:'#fff',
          alignSelf : 'center',
          marginLeft: 10
      }
 
  });
  export default styles;
  