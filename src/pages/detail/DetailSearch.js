import React,{ useState, useEffect } from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import styles from './Detail-styles';
import saveItem from '../../service/saveItem/saveItem';
import getItem from '../../service/saveItem/getItem';
import getDataDetail from '../../service/api/getDataDetail';
import deleteSaveItem from '../../service/saveItem/deleteSaveItem';
import Icons from 'react-native-vector-icons/Ionicons'
import Loading from '../../components/loading/Loading'


function DetailSearch({route}) {
    const {detailItem} = route.params;
    const [detailSearchItem, setDetailSearchItem] = useState({}) ;
    const [dataFav, setDataFav] = useState([]) ;
    const existed_Favorite= dataFav.find(tes=> detailItem.id === tes.id)
    const [loading, setLoading] = useState(false)

    useEffect(()=>{
        getFavData();
    }, [dataFav])

    useEffect(()=>{
        getInitData();
    }, [])

    const getInitData = async() => {
        setLoading(true);
        const resp = await getDataDetail(detailItem.id); 
        setDetailSearchItem(resp);
        setLoading(false)        
    }

    const getFavData = async() => {
        const resp = await getItem(); 
        setDataFav(resp);
    }
    const handleFav = async() => {
        if(existed_Favorite){
            deleteSaveItem(detailSearchItem);
        } else {
            saveItem(detailSearchItem);
        }
    }

    return (
        <>
        <ScrollView>                        
        {loading==false?(
            <View>
                <View style={styles.topContainer}>
                    <View style={styles.creditContainer}>
                        <Text style={styles.titleFont}>Art Detail</Text>
                        <TouchableOpacity style={styles.favContainer}
                            onPress={()=>handleFav()}>
                            {existed_Favorite?(<Icons name='heart' color={'#0A1A1D'} size={30} style={{}}/> ):(<Icons name='heart-outline' color={'#0A1A1D'} size={30} style={{alignSelf:'center', justifyContent:'center'}}/> )}
                        </TouchableOpacity>
                    </View>
                    <Image
                        style={styles.image}
                        source={{ uri: `https://www.artic.edu/iiif/2/${detailSearchItem.image_id}/full/843,/0/default.jpg` }}
                    />
                </View>
                    <View style={styles.textContainer}>
                        <View style={styles.textSubContainer}>
                            <Text style={styles.fontSubTitle}>Title</Text>
                            <Text style={styles.fontSub}>{detailSearchItem.title}</Text>
                        </View>
                        <View style={styles.textSubContainer}>
                            <Text style={styles.fontSubTitle}>Inscription</Text>
                            <Text style={styles.fontSub}>{detailSearchItem.inscriptions?(detailSearchItem.inscriptions):('Not Available')}</Text>
                        </View>
                        <View style={styles.textSubContainer}>
                            <Text style={styles.fontSubTitle}>Provenance text</Text>
                            <Text style={styles.fontSub}>{detailSearchItem.provenance_text?(detailSearchItem.provenance_text):('Not Available')}</Text>
                        </View>
                        <View style={styles.textSubContainer}>
                            <Text style={styles.fontSubTitle}>Publication History</Text>
                            <Text style={styles.fontSub}>{detailSearchItem.publication_history?(detailSearchItem.publication_history):('Not Available')}</Text>
                        </View>
                        <View style={styles.textSubContainer}>
                            <Text style={styles.fontSubTitle}>Exhibition history</Text>
                            <Text style={styles.fontSub}>{detailSearchItem.exhibition_history?(detailSearchItem.exhibition_history):('Not Available')}</Text>
                        </View>
                    </View>
                
            </View>
        
        ):(
            <View>
                <Loading />
            </View>
        )}
        </ScrollView>
        </>
        
        
    )
}

export default DetailSearch;