import React,{ useState, useEffect } from 'react';
import {View, Text, Image, ScrollView, TouchableOpacity} from 'react-native';
import styles from './Detail-styles';
import saveItem from '../../service/saveItem/saveItem';
import getItem from '../../service/saveItem/getItem';
import deleteSaveItem from '../../service/saveItem/deleteSaveItem';
import Icons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

function Detail({route}) {
    const [{detailItem}, setDetailItem] = useState(route.params) ;
    const [dataFav, setDataFav] = useState([]) ;
    const existed_Favorite= dataFav.find(tes=> detailItem.id === tes.id)

    useEffect(()=>{
        getFavData();
    }, [dataFav])

    const getFavData = async() => {   
        try {
            const value = await AsyncStorage.getItem('savedItemz')
            if(value !== null) {
                let resp = JSON.parse(value);  
                setDataFav(resp);
                console.log("Hit")
            } 
          } catch(e) {
              console.error(e) 
          }
    }

    const handleFav = async() => {
        if(existed_Favorite){
            deleteSaveItem(detailItem);
        } else {
            saveItem(detailItem);
        }
    }

    return (
        
        <View>
            <View style={styles.topContainer}>
                <View style={styles.creditContainer}>
                    <Text style={styles.titleFont}> Art Detail</Text>
                    <TouchableOpacity style={styles.favContainer}
                        onPress={()=>handleFav()}>
                        {existed_Favorite?(<Icons name='heart' color={'#0A1A1D'} size={30} style={{}}/> ):(<Icons name='heart-outline' color={'#0A1A1D'} size={30} style={{alignSelf:'center', justifyContent:'center'}}/> )}
                    </TouchableOpacity>
                </View> 
                
                <Image
                    style={styles.image}
                    source={{ uri: `https://www.artic.edu/iiif/2/${detailItem.image_id}/full/843,/0/default.jpg` }}
                />
            </View>
            <ScrollView>
                <View style={styles.textContainer}>
                    <View style={styles.textSubContainer}>
                        <Text style={styles.fontSubTitle}>Title</Text>
                        <Text style={styles.fontSub}>{detailItem.title}</Text>
                    </View>
                    <View style={styles.textSubContainer}>
                        <Text style={styles.fontSubTitle}>Inscription</Text>
                        <Text style={styles.fontSub}>{detailItem.inscriptions?(detailItem.inscriptions):('Not Available')}</Text>
                    </View>
                    <View style={styles.textSubContainer}>
                        <Text style={styles.fontSubTitle}>Provenance text</Text>
                        <Text style={styles.fontSub}>{detailItem.provenance_text?(detailItem.provenance_text):('Not Available')}</Text>
                    </View>
                    <View style={styles.textSubContainer}>
                        <Text style={styles.fontSubTitle}>Publication History</Text>
                        <Text style={styles.fontSub}>{detailItem.publication_history?(detailItem.publication_history):('Not Available')}</Text>
                    </View>
                    <View style={styles.textSubContainer}>
                        <Text style={styles.fontSubTitle}>Exhibition history</Text>
                        <Text style={styles.fontSub}>{detailItem.exhibition_history?(detailItem.exhibition_history):('Not Available')}</Text>
                    </View>          
                    
                    
                </View>
                </ScrollView>
        </View>
    )
}

export default Detail;