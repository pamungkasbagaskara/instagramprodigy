import React,{ useState, useEffect } from 'react';
import {View, Text, Button, FlatList, Image, TouchableHighlight, StyleSheet, TextInput, ActivityIndicator, RefreshControl} from 'react-native';
import getDataPage from '../../service/api/getData';
import Loading from '../../components/loading/Loading'
import getSearchDataPage from '../../service/api/getSearchData';
import SearchBar from '../../components/searchBar/SearchBar'
import styles from './Home-style'

function Home({ navigation }) {
    const [currentData, setCurrentData] = useState([]) ;
    const [currentSearchData, setCurrentSearchData] = useState([]) ;
    const [pages, setPages] = useState(2) ;
    const [searchPages, setSearchPages] = useState(2) ;
    const [loading, setLoading] = useState(false)
    const [loadingFooter, setLoadingFooter] = useState(false)
    const [searchText, setSearchText] = useState('');
    const [refreshing, setRefreshing] = useState(false)

    useEffect(()=>{
        getInitialData();
    }, [])

    const _onRefresh = () => {
        setRefreshing(true);
        setSearchText('')
        getInitialData();
    };

    const getInitialData = async() => {
        setLoading(true);
        const resp = await getDataPage(1);
        setCurrentData(resp);
        setLoading(false);
        setRefreshing(false);
    }

    const getSearchDatas = async() => {
        setLoading(true);
        const resp = await getSearchDataPage(searchText, 1);
        setCurrentSearchData(resp);
        setLoading(false);
    }
    const nextPageSearchData = async() => {
        setLoadingFooter(true);
        setSearchPages(searchPages+1);
        const resp = await getSearchDataPage(searchText, searchPages);
        let pageNew = currentSearchData.concat(resp) 
        setCurrentSearchData(pageNew)
        setLoadingFooter(false);
    }

    const nextPageData = async() => {
        setLoadingFooter(true);
        setPages(pages+1);
        const resp = await getDataPage(pages);
        let pageNew = currentData.concat(resp) 
        setCurrentData(pageNew)
        setLoadingFooter(false);
    }

	renderItem = ({ item }) => {
		return (            
            <TouchableHighlight 
                onPress={() => navigation.navigate('Detail',{detailItem : item}) }
                style={{ flex: 1, flexDirection: 'column', margin: 5, borderRadius:20 }}>	
                <Image
                    style={{justifyContent: 'center', alignItems: 'center', height: 150, borderRadius:30}}
                    source={{ uri:`https://www.artic.edu/iiif/2/${item.image_id}/full/843,/0/default.jpg` }}
                /> 
            </TouchableHighlight>  
        );
	  }
      renderItemSearch = ({ item }) => {
		return (
            <TouchableHighlight 
                onPress={() => navigation.navigate('DetailSearch',{detailItem : item}) }
                style={{ flex: 1, flexDirection: 'column',margin: 5, borderRadius:20 }}>
                {item.thumbnail !== null ?(
                    <Image
                        style={{justifyContent: 'center', alignItems: 'center', height: 150, borderRadius:30}}
                        source={{ uri: item.thumbnail.lqip }}
                    />
                ):(
                    <Text>{"Data Not Complete"}</Text>
                )}    	

            </TouchableHighlight> 
                                
           
        );
	  }
    const renderFooter = () => {
    return (
        <View style={StyleSheet.absoluteFillObject}>
        {loadingFooter ? (
            <ActivityIndicator
            color="black"
            style={{margin: 15}} />
        ) : null}
        </View>
    );
    };


    return (
        <>
        {loading == false ? (
            <View>
                <View style={styles.topContainer}>
                    <Text style={styles.titleFont}>{"Art Gallery"}</Text>
                    <SearchBar 
                        onChangeText={newText => setSearchText(newText)}
                        onPress={() => getSearchDatas()}
                    />
                </View>
                {searchText==''?(
                    <FlatList
                        data={currentData} 
                        renderItem={renderItem}
                        numColumns={3}
                        keyExtractor={item => item.id}
                        onEndReached={nextPageData}
                        refreshControl={
                        <RefreshControl 
                            refreshing={refreshing} 
                            onRefresh={_onRefresh}
                            tintColor="#F8852D"/>
                        }
                        ListFooterComponent ={renderFooter}
                    />
                ):(
                    <View>                
                        {currentSearchData !== null?(                
                            <FlatList
                                data={currentSearchData}
                                renderItem={renderItemSearch}
                                numColumns={3}
                                keyExtractor={item => item.id}
                                onEndReached={nextPageSearchData}
                                refreshControl={
                                    <RefreshControl 
                                        refreshing={refreshing} 
                                        onRefresh={_onRefresh}
                                        tintColor="#F8852D"/>
                                    }
                                ListFooterComponent ={renderFooter}
                                />
                                ):(
                                    <Text>{"No Data"}</Text>
                        )}
                    </View>

                )}
            </View>
            
        ) :(         
            <>   
                <Loading />            
            </>
        )} 

        </>
        
    )
}

export default Home;