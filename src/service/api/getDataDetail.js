import fetchAxios from './fetchAxios';

const getDataDetail= async (tes)=>{    
    try{
        const response = await fetchAxios("GET", `/${tes}`,{}, {})
        return response.data.data;   
    }catch(error){
        console.error(error);
    }
    }

export default getDataDetail;