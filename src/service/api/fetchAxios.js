import axios from 'axios';

async function fetchAxios(method, path, body, header) {
  try {
    const req = await axios({
      method,
      url: `https://api.artic.edu/api/v1/artworks${path}`,
      data: body,
      headers: header,      
    });

    if (req.status !== 400) {
      return req;
    }
  } catch (e) {
    throw e;
  }

}

export default fetchAxios;