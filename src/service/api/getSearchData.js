import fetchAxios from './fetchAxios';

const getSearchDataPage= async (text, page)=>{    
    try{
        const response = await fetchAxios("GET", `/search?q=${text}&page=${page}&limit=10`,{}, {})
        return response.data.data;   
    }catch(error){
        console.error(error);
    }
    }

export default getSearchDataPage;
