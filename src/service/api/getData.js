import fetchAxios from './fetchAxios';

const getDataPage= async (page)=>{    
    try{
        const response = await fetchAxios("GET", `?page=${page}&limit=15`,{}, {})
        return response.data.data;   
    }catch(error){
        console.error(error);
    }
    }

export default getDataPage;