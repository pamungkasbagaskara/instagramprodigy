import React,{ useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const saveItem = async(toBeSaved) => {
    AsyncStorage.getItem('savedItemz', (err, result) => {
        if (result !== null) {
            let temp = JSON.parse(result)
            let newSaved = temp.concat(toBeSaved);
            AsyncStorage.setItem('savedItemz', JSON.stringify(newSaved));
        } else {
            console.log('Data Not Found');
            let dummy = []
            let newSaved2 = dummy.concat(toBeSaved)
            AsyncStorage.setItem('savedItemz', JSON.stringify(newSaved2));
        }
        });        
}

export default saveItem;