import React,{ useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const getItem = async() => {
    try {
        const value = await AsyncStorage.getItem('savedItemz')
        if(value !== null) {
            return JSON.parse(value);  
        } 
      } catch(e) {
          console.error(e) 
      }
}

export default getItem;