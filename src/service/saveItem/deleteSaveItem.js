import AsyncStorage from '@react-native-async-storage/async-storage';

const deleteSaveItem = async(toBeDelete) => {
    AsyncStorage.getItem('savedItemz', (err, result) => {
        if (result !== null) {
            var newFav = JSON.parse(result).filter(function (el) {
                return el.id !== toBeDelete.id
              });
            AsyncStorage.setItem('savedItemz', JSON.stringify(newFav));
        } else {
            console.log('Data Not Found');
        }
        });        
}

export default deleteSaveItem;