import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View, Image, Modal } from 'react-native'
import LottieView from 'lottie-react-native';


export default function Loading() {
    return (      
    <View style={[StyleSheet.absoluteFillObject, styles.container]}>
        <LottieView source={require('../../assets/99844-loading.json')} autoPlay loop />

    </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%'
    },
    text: {
        fontSize: 18,
        color: 'white'
    },
    modalBackground: {
        position : 'absolute',
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
      },
      activityIndicatorWrapper: {
          position : 'absolute',
        backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        zIndex:10,
      },
      container :{
          position : 'absolute',
          zIndex :1,
          justifyContent :'center',
          alignItems : 'center',
          backgroundColor : 'rgba(0,0,0,0.3'
      }
})
