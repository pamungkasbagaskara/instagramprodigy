import React from 'react'
import { Dimensions, StyleSheet, Text, View, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
const { width, height } = Dimensions.get('window');


export default function SearchBar({onChangeText, onPress}) {
    return (      
        <View style={{flexDirection:'row', justifyContent:'center', marginBottom:20,}}>
            <View style={styles.search}>
                <Ionicons name="search" size={18} color={'grey'} />
                <TextInput
                    onChangeText={onChangeText
                    } 
                    placeholder={'Search Art...'}
                    style={{ width: width, marginLeft: 20 }}
                />
            </View>
            <View style={styles.button}>
                <Text 
                onPress={onPress}
                style={styles.font}
                >{"Search"}</Text>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    search: {
        borderRadius: 100,
        borderColor: '#dbdbdb',
        borderWidth: 1,
        margin: 5,
        width: width * 0.7,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        paddingHorizontal: 10
      },
      button :{
        width: width * 0.2,
        backgroundColor: '#8ecae6',
        height: 40,
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 50,
        margin: 5,
      },
      font :{
          color:'#283618'
      }
})
